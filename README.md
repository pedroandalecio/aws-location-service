# Amazon Location Service | Reverse Geocoding

API that receives latitude and longitude and returns the nearest locations using Amazon Location Service.

You'll need to define these Environment Variables:

- AWS_ACCESS_KEY_ID = "YOUR ACCESS KEY";
- AWS_SECRET_ACCESS_KEY = "YOUT SECRET ACCESS KEY";
- AWS_LOCATION_INDEX_NAME = "INDEX CREATED AT AWS LOCATION SERVICE";
- AWS_REGION_NAME = "REGION NAME OF THE INDEX"

https://docs.aws.amazon.com/location/latest/developerguide/search-place-index-reverse-geocode.html


