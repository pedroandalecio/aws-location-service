package com.poc.awslocationservices.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.poc.awslocationservices.model.PlaceDTO;
import com.poc.awslocationservices.service.AWSLocationService;
import java.util.List;

@RestController
@RequestMapping("location")
@RequiredArgsConstructor
public class LocationsController {

    private final AWSLocationService service;

    @GetMapping
    public List<PlaceDTO> findAllPlacesByLongitudeLatitude(@RequestParam Double longitude,
                                                           @RequestParam Double latitude){
        return service.findPlacesByLongitudeLatitude(longitude, latitude);
    }

}
