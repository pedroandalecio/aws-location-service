package com.poc.awslocationservices.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import com.poc.awslocationservices.config.AppConfig;
import com.poc.awslocationservices.model.PlaceDTO;
import software.amazon.awssdk.services.location.LocationClient;
import software.amazon.awssdk.services.location.model.SearchForPositionResult;
import software.amazon.awssdk.services.location.model.SearchPlaceIndexForPositionRequest;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AWSLocationService {

    private final LocationClient locationClient;
    private final AppConfig appConfig;

    public List<PlaceDTO> findPlacesByLongitudeLatitude(Double longitude, Double latitude) {
        var request = SearchPlaceIndexForPositionRequest.builder()
                .position(longitude, latitude)
                .indexName(appConfig.getAwsLocationIndexName())
                .maxResults(1)
                .build();

        return locationClient
                .searchPlaceIndexForPosition(request)
                .results()
                .stream()
                .map(SearchForPositionResult::place)
                .map(PlaceDTO::of)
                .collect(Collectors.toList());
    }

}
