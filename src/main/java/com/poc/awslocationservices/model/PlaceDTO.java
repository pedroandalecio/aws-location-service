package com.poc.awslocationservices.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import software.amazon.awssdk.services.location.model.Place;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PlaceDTO {
    
	private String addressNumber;
    private String country;
    private Boolean interpolated;
    private String label;
    private String municipality;
    private String neighborhood;
    private String postalCode;
    private String region;
    private String street;
    private String subRegion;
    
    public static PlaceDTO of(Place place) {
        return builder()
                .addressNumber(place.addressNumber())
                .country(place.country())
                .label(place.label())
                .municipality(place.municipality())
                .neighborhood(place.neighborhood())
                .postalCode(place.postalCode())
                .region(place.region())
                .street(place.street())
                .subRegion(place.subRegion())
                .build();
    }
}
